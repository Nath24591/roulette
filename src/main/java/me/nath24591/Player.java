package me.nath24591;

import java.util.*;

public class Player {
    private static HashMap<String,Player> playerInstances = new HashMap<>();
    private String name;
    private long DEFAULT_STARTING_AMOUNT = 3000;
    private long balance;
    private long gamesPlayed;
    private long gamesWon;
    private long gamesLost;

      static Player getPlayerInstance(String playerName){
         Player player;
        if(playerInstances == null || playerInstances.get(playerName) == null){
            player = new Player(playerName);
            playerInstances.put(playerName,player);
        } else {
            player = playerInstances.get(playerName);
        }
        return player;
    }


    private Player(String name) {
        this.name = name;
        this.balance = DEFAULT_STARTING_AMOUNT;
        this.gamesPlayed = 0;
        this.gamesLost = 0;
        this.gamesWon = 0;
    }

    public long getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(long gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public long getGamesWon() {
        return gamesWon;
    }

    public void setGamesWon(long gamesWon) {
        this.gamesWon = gamesWon;
    }

    public long getGamesLost() {
        return gamesLost;
    }

    public void setGamesLost(long gamesLost) {
        this.gamesLost = gamesLost;
    }

    long getBalance() {
        return balance;
    }

    void setBalance(long balance) {
        this.balance = balance;
    }

    String getName() {
        return name;
    }
}
