package me.nath24591;

import java.util.Random;

public class RouletteResult {
    private Random random = new Random();
    private long selectedNumber;
    private long winningNumber;
    private boolean winningBet;

    RouletteResult(long selectedNumber) {
        this.selectedNumber = selectedNumber;
        this.winningNumber  = generateWinningNumber();
        this.winningBet = this.winningNumber == this.selectedNumber;
    }

    public long getSelectedNumber() {
        return selectedNumber;
    }

    public void setSelectedNumber(long selectedNumber) {
        this.selectedNumber = selectedNumber;
    }

    long getWinningNumber() {
        return winningNumber;
    }

    public void setWinningNumber(long winningNumber) {
        this.winningNumber = winningNumber;
    }

    boolean isWinningBet() {
        return winningBet;
    }

    public void setWinningBet(boolean winningBet) {
        this.winningBet = winningBet;
    }

    private long generateWinningNumber(){
        return random.nextInt(36 + 1);
    }

}
