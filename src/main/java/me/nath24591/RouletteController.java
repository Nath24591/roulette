package me.nath24591;

import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class RouletteController {

    @RequestMapping("/")
    public String index() {
        return "Roulette main page";
    }

    @PostMapping("/createPlayerInstance")
    public String create(@RequestBody Map<String, String> body) {
        String playerName = body.get("playerName");

        Player player = Player.getPlayerInstance(playerName);
        return player.getName();
    }

    @PostMapping("/placeBet")
    public String placeBet(@RequestBody Map<String, String> body) {
        String playerName = body.get("playerName");
        long betAmount;
        long selectedNumber;
        try {
            betAmount = Long.valueOf(body.get("betAmount"));
            selectedNumber = Long.valueOf(body.get("selectedNumber"));
        } catch (NumberFormatException e){
            return "Please ensure you enter numerical values for your bet and selected number";
        }

        if(selectedNumber < 1 || selectedNumber > 36) {
            return "Please bet on a numbers 1-36";
        }

        if(betAmount < 1) {
            return "You must bet more then 0";
        }

        Player player = Player.getPlayerInstance(playerName);
        return Roulette.placeBet(player, selectedNumber, betAmount);
    }

    @GetMapping("/getPlayerBalance/{playerName}")
    public long getPlayerBalance(@PathVariable String playerName) {
        Player player = Player.getPlayerInstance(playerName);
        return player.getBalance();
    }

}
