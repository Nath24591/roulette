package me.nath24591;

class Roulette {
    static String placeBet(Player player, long selectedNumber, long betAmount){
        String response;
        //Check if user has enough money to place bets;
        if(player.getBalance() < betAmount){
            return "You do not have sufficient balance to place that bet";
        }

        RouletteResult rouletteResult = new RouletteResult(selectedNumber);

        long playerBalanceChange;
        if(rouletteResult.isWinningBet()){
            //Calculate winning amount
            playerBalanceChange = selectedNumber * betAmount;
        } else {
            playerBalanceChange = betAmount;
        }

        updatePlayerBalance(player,rouletteResult, playerBalanceChange);
        response = "The winning number is: " + rouletteResult.getWinningNumber();
        if(rouletteResult.isWinningBet()){
            response = response + "\nYou won £" + playerBalanceChange;
        } else {
            response = response + "\nYou lost £ " + playerBalanceChange;
        }
        response = response + "\nYour new balance is £" + player.getBalance();
        return response;
    }

    private static void updatePlayerBalance(Player player, RouletteResult rouletteResult, long playerBalanceChange){
        if(rouletteResult.isWinningBet()){
            player.setBalance(player.getBalance() + playerBalanceChange);
        } else {
            player.setBalance(player.getBalance() - playerBalanceChange);
        }
    }
}
