Simple roulette game, players start with £3000, and can place bets until they run out of money. A winning bet wins bet amount * selected number

Currently available API's

POST /createPlayerInstance - This expects a post request containing e.g. {"playerName" : "Bob"}  
POST /placeBet - This expects a post request containing e.g. {"playerName" : "Bob", "betAmount" : "400", "selectedNumber" : 32}  
GET /getPlayerBalance/{playerName} - This expects a get request containing e.g. localhost:8080/getPlayerBalance/Bob
